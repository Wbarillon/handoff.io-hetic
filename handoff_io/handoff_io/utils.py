import sys
import os
import importlib.util

def import_every_files_from(directory):
    '''
        With this function, one can import every files within a directory.
    '''
    for filename in [filename for filename in os.listdir(directory) if '__' not in filename and filename.endswith('.py')]:
        file_path = directory / filename
        module_name = filename[:-3]
        spec = importlib.util.spec_from_file_location(module_name, file_path)
        module = importlib.util.module_from_spec(spec)
        sys.modules[module_name] = module
        spec.loader.exec_module(module)

def save_django_object(instance, data):
    for key, value in data.items():
        setattr(instance, key, value)
    instance.save()

    return instance

def dictfetchall(cursor):
    '''
        Returns all rows from a cursor as a dict
    '''
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]