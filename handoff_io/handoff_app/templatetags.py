from django import template

register = template.Library()

@register.filter
def default_if_zero(value):
    if value == 0 or value == '0':
        return '-'
    else:
        return value

@register.filter
def float_to_int(value):
    if str(value).endswith(',0') or str(value).endswith('.0'):
        return str(value)[:-2]
    else:
        return value