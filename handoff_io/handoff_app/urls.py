from django.urls import include, path

from handoff_app.views.index import (
    authentication,
    home,
    mockup
)

webpages_patterns = [
    path('', home, name = 'home'),
    path('mockup', mockup, name = 'mockup')
]

forms_patterns = [
    path('authentication', authentication, name = 'authentication')
]

urlpatterns = [
    path('', include(webpages_patterns)),
    path('', include(forms_patterns))
]