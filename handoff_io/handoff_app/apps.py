from django.apps import AppConfig


class HandoffAppConfig(AppConfig):
    name = 'handoff_app'
