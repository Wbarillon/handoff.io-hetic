window.addEventListener('load', () => {

    messageList = document.querySelector('.list-group')
    specForms = document.querySelectorAll('#js-spec-form')
    canvas = document.getElementById('mockup-canvas')
    canvasEdges = canvas.getBoundingClientRect()
    offsetX = canvasEdges.x
    offsetY = canvasEdges.y


    isDown = false
    isMoving = false

    ctx = canvas.getContext('2d')
    ctx.canvas.width = document.getElementById('mockup').width
    ctx.canvas.height  = document.getElementById('mockup').height
    ctx.lineWidth = 2;
    ctx.shadowColor = '#7E42FF';
    ctx.shadowBlur = 6;
    ctx.shadowOffsetY = "3";
    var dash = 8;
    ctx.setLineDash([dash, 8]);


    descriptionAreas = JSON.parse(document.getElementById('description_areas').textContent)
    idDescriptionArea = JSON.parse(document.getElementById('id_description_area').textContent)

    // draw description areas from db at page load
    for (i = 0; i < descriptionAreas.length; i++) {
        if (descriptionAreas[i].id == idDescriptionArea) {
            rectangleLocalization = {
                x_axis: descriptionAreas[i].x_axis,
                y_axis: descriptionAreas[i].y_axis,
                width: descriptionAreas[i].width,
                height: descriptionAreas[i].height
            }

            rectangleLocalization = JSON.stringify(rectangleLocalization)

            specForms.forEach((form) => {
                form.querySelector('input[name="django_object_id"]').value = descriptionAreas[i].id
                form.querySelector('input[name="rectangleLocalization"]').value = rectangleLocalization
            })
            ctx.strokeStyle = "orange"
            ctx.strokeRect(descriptionAreas[i].x_axis, descriptionAreas[i].y_axis, descriptionAreas[i].width, descriptionAreas[i].height)
            messageList.classList.remove('hidden')
        }
        else {
            ctx.strokeStyle = "#D87AFA"
            ctx.strokeRect(descriptionAreas[i].x_axis, descriptionAreas[i].y_axis, descriptionAreas[i].width, descriptionAreas[i].height)
        }
    }

    pointInRect = (x, y, rect) => {
        if (x < rect.x_axis || x > rect.x_axis + rect.width || y < rect.y_axis || y > rect.y_axis + rect.height) {
            return false
        }
        else {
            return true
        }
    }

    saveRectangleLocalization = () => {

        // rectangle drawn from top left to botton right
        if (width > 0 & height > 0) {
            topLeftCoordonates = {
                x_axis: startX,
                y_axis: startY
            }
        }
        // rectangle drawn from top right to bottom left
        else if (width < 0 & height > 0) {
            topLeftCoordonates = {
                x_axis: endX,
                y_axis: startY
            }
        }
        // rectangle drawn from bottom left to top right
        else if (width > 0 & height < 0) {
            topLeftCoordonates = {
                x_axis: startX,
                y_axis: endY
            }
        }
        // rectangle drawn from bottom right to top left
        else if (width < 0 & height < 0) {
            topLeftCoordonates = {
                x_axis: endX,
                y_axis: endY
            }
        }

        // use of ... operator to merge a dictionnary into another one
        rectangleLocalization = {
            ...topLeftCoordonates,
            width: Math.abs(width),
            height: Math.abs(height)
        }

        rectangleLocalization = JSON.stringify(rectangleLocalization)
        specForms.forEach((form) => {
            form.querySelector('input[name="rectangleLocalization"]').value = rectangleLocalization
        })
    }

    handleMouseDown = (event) => {
        event.preventDefault()
        event.stopPropagation()

        //ctx.strokeStyle = "#D87AFA"

        // save the starting x/y of the rectangle
        startX = event.clientX - offsetX
        startY = event.clientY - offsetY

        // set a flag indicating the drag has begun
        isDown = true
    }

    handleMouseMove = (event) => {
        event.preventDefault()
        event.stopPropagation()

        canvas.removeEventListener('click', selectAreaDescription)

        isMoving = true
        
        // if we're not dragging, just return
        if (isDown === false) {
            return
        }

        // get the current mouse position
        endX = event.clientX - offsetX
        endY = event.clientY - offsetY

        // Put your mousemove stuff here
        // clear the canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        ctx.strokeStyle = "#D87AFA"
        descriptionAreas.forEach((description_area) => {
            ctx.strokeRect(description_area.x_axis, description_area.y_axis, description_area.width, description_area.height)
        })

        // calculate the rectangle width/height based
        // on starting vs current mouse position
        width = endX - startX
        height = endY - startY

        // draw a new rect from the start position 
        // to the current mouse position
        
        ctx.strokeStyle = "green"
        ctx.strokeRect(startX, startY, width, height)

    }

    handleMouseUp = (event) => {
        event.preventDefault()
        event.stopPropagation()

        if (isMoving === true) {
            saveRectangleLocalization()
        }

        // the drag is over, clear the dragging flag
        isDown = false
    }

    handleMouseOut = (event) => {
        event.preventDefault()
        event.stopPropagation()

        saveRectangleLocalization()
    
        // the drag is over, clear the dragging flag
        isDown = true
    }

    descriptionAreaSelected = (event) => {

        cursorX = event.clientX - offsetX
        cursorY = event.clientY - offsetY

        for (i = 0; i < descriptionAreas.length; i++) {
            if (pointInRect(cursorX, cursorY, descriptionAreas[i])) {
                return true
            }
        }
    }

    selectSmallestRectangle = (event) => {
        rectanglesConflict = {}
        rectanglesArea = []

        cursorX = event.clientX - offsetX
        cursorY = event.clientY - offsetY

        for (i = 0; i < descriptionAreas.length; i++) {
            if (pointInRect(cursorX, cursorY, descriptionAreas[i])) {
                rectanglesConflict[i] = descriptionAreas[i]
            }
        }

        for (i = 0; i < Object.keys(rectanglesConflict).length; i++) {
            rectanglesArea[i] = {
                index: Object.keys(rectanglesConflict)[i],
                area: Object.values(rectanglesConflict)[i].height * Object.values(rectanglesConflict)[i].width
            }
        }

        tabAreas = []

        rectanglesArea.forEach((rectangleArea) => {
            tabAreas.push(rectangleArea['area'])
        })

        smallestArea = tabAreas.sort((a, b) => a - b)[0]

        for (i = 0; i < rectanglesArea.length; i++) {
            if (rectanglesArea[i]['area'] == smallestArea) {
                index = rectanglesArea[i].index
            }
        }

        for (i = 0; i < descriptionAreas.length; i++) {
            if (i == index) {
                ctx.strokeStyle = "orange"
                ctx.strokeRect(descriptionAreas[i].x_axis, descriptionAreas[i].y_axis, descriptionAreas[i].width, descriptionAreas[i].height)

                selectedDescriptionArea = descriptionAreas[i]
            }
        }

        return selectedDescriptionArea
    }

    relations_url_get = '/mockup'

    getCookie = (name) => {
        cookies = decodeURIComponent(document.cookie).split(';')
    
        for (i = 0; i < cookies.length; i++) {
            keyValueArray = cookies[i].split("=")
            if (keyValueArray[0] == name) {
                return keyValueArray[1]
            }
        }
    }

    csrftoken = getCookie("csrftoken")

    getQuery = (params) => {
        return Object.keys(params)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
            .join('&')
    }

    selectAreaDescription = (event) => {
        if (descriptionAreaSelected(event)) {

            messageList.classList.remove('hidden')

            selectedDescriptionArea = selectSmallestRectangle(event)

            rectangleLocalization = {
                x_axis: selectedDescriptionArea.x_axis,
                y_axis: selectedDescriptionArea.y_axis,
                width: selectedDescriptionArea.width,
                height: selectedDescriptionArea.height
            }
    
            rectangleLocalization = JSON.stringify(rectangleLocalization)

            specForms.forEach((form) => {
                form.querySelector('input[name="django_object_id"]').value = selectedDescriptionArea.id
                form.querySelector('input[name="rectangleLocalization"]').value = rectangleLocalization
            })

            parameters = {
                action: 'selected_description_area',
                django_object_id: JSON.stringify(selectedDescriptionArea.id),
                rectangleLocalization: rectangleLocalization
            }

            query = getQuery(parameters)

            fetch(relations_url_get, {
                method: 'POST',
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                    "X-CSRFToken": csrftoken
                },
                body: query
            })
            .then((response) => {
                if (response.status === 200) {
                    window.location.replace(relations_url_get)
                }
            })
        }
        else {
            ctx.clearRect(0, 0, canvas.width, canvas.height)
            ctx.strokeStyle = "#D87AFA"
            descriptionAreas.forEach((description_area) => {
                ctx.strokeRect(description_area.x_axis, description_area.y_axis, description_area.width, description_area.height)
            })
            messageList.classList.add('hidden')
            messageList.classList.remove('list-group')
        }
    }

    if (descriptionAreas.length > 0) {
        canvas.addEventListener('click', selectAreaDescription)
    }
    
    canvas.addEventListener('mousedown', (event) => {
        handleMouseDown(event)
        canvas.addEventListener('mousemove', handleMouseMove)
    })

    canvas.addEventListener('mouseup', (event) => {
        handleMouseUp(event)
        canvas.removeEventListener('mousemove', handleMouseMove)
    })

    if (isDown === true) {
        canvas.addEventListener('mouseout', handleMouseOut)
    }
})