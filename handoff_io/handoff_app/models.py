from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser
from django.db import models
from django.utils import timezone

from handoff_app.managers import (
    CustomUserManager
)


# Create your models here.

class CustomUser(PermissionsMixin, AbstractBaseUser):
    nickname = models.CharField(verbose_name = 'Pseudo', max_length = 50, unique = True)
    email = models.EmailField(unique = True)
    is_staff = models.BooleanField(default = False)
    is_active = models.BooleanField(default = True)
    date_joined = models.DateTimeField(verbose_name = 'Date de création', default = timezone.now)

    USERNAME_FIELD = 'nickname'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email']

    objects = CustomUserManager()

    def __str__(self):
        return self.nickname

    class Meta:
        verbose_name = 'Utilisateur (custom)'
        verbose_name_plural = 'Utilisateurs (custom)'


class Organisation(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 50, verbose_name = 'Nom', null = True, blank = True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Organisation'
        verbose_name_plural = 'Organisations'


class Projet(models.Model):
    id = models.AutoField(primary_key = True)
    id_organisation = models.ForeignKey('Organisation', on_delete = models.CASCADE)
    name = models.CharField(max_length = 50, verbose_name = 'Nom', null = True, blank = True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Projet'
        verbose_name_plural = 'Projets'

class Maquette(models.Model):
    id = models.AutoField(primary_key = True)
    id_projet = models.ForeignKey('Projet', on_delete = models.CASCADE)
    name = models.CharField(max_length = 50, verbose_name = 'Nom', null = True, blank = True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Maquette'
        verbose_name_plural = 'Maquettes'

class DescriptionArea(models.Model):
    id = models.AutoField(primary_key = True)
    id_maquette = models.ForeignKey('Maquette', on_delete = models.CASCADE)
    x_axis = models.FloatField(null = True, blank = True)
    y_axis = models.FloatField(null = True, blank = True)
    width = models.IntegerField(null = True, blank = True)
    height = models.IntegerField(null = True, blank = True)

    def __str__(self):
        return self.str(id)

    class Meta:
        verbose_name = 'Zone de description'
        verbose_name_plural = 'Zones de description'

class TextComment(models.Model):
    id = models.AutoField(primary_key = True)
    id_description_area = models.ForeignKey('DescriptionArea', on_delete = models.CASCADE)
    comment_content = models.TextField(verbose_name = 'Contenu commentaire', null = True, blank = True)

    def __str__(self):
        return self.comment_content

    class Meta:
        verbose_name = 'Commentaire textuel'
        verbose_name_plural = 'Commentaires textuels'

class AudioComment(models.Model):
    id = models.AutoField(primary_key = True)
    id_description_area = models.ForeignKey('DescriptionArea', on_delete = models.CASCADE)
    audio_content = models.FileField(verbose_name = 'Contenu audio', null = True, blank = True)

    def __str__(self):
        return self.id_description_area.id_maquette.name

    class Meta:
        verbose_name = 'Commentaire audio'
        verbose_name_plural = 'Commentaires audios'

class Ressource(models.Model):
    id = models.AutoField(primary_key = True)
    id_description_area = models.ForeignKey('DescriptionArea', on_delete = models.CASCADE)
    resource_content = models.FileField(verbose_name = 'Ressources', null = True, blank = True)

    def __str__(self):
        return self.id_description_area.id_maquette.name

    class Meta:
        verbose_name = 'Ressource'
        verbose_name_plural = 'Ressources'
