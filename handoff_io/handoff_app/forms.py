from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

from handoff_app.models import (
    CustomUser,
    DescriptionArea,
    TextComment
)


class ActionForm(forms.Form):

    django_object_id = forms.IntegerField(required = False)
    action = forms.CharField()

class InscriptionForm(UserCreationForm):

    class Meta:
        model = CustomUser
        fields = ['nickname', 'email', 'password1', 'password2']

class TextInput(forms.Form):
    description = forms.CharField(
        label = 'Description',
        widget = forms.Textarea,
        required = False
    )

class AudioInput(forms.Form):
    audio = forms.FileField()
    pst = forms.CharField()

class DescriptionAreaModelForm(forms.ModelForm):

    class Meta:
        model = DescriptionArea
        fields = ['id_maquette', 'x_axis', 'y_axis', 'width', 'height']

class TextCommentModelForm(forms.ModelForm):

    class Meta:
        model = TextComment
        fields = ['id_description_area', 'comment_content']
