from handoff_app.views.services.home import (
    reteste,
    teste
)


def home_controller(request, context):

    context = reteste(context)

    context = teste(context)
    
    return context