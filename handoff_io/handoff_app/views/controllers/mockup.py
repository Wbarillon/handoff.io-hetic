from handoff_app.forms import ActionForm
from handoff_app.models import DescriptionArea
from handoff_app.views.services.mockup import (
    get_description_areas_from_db,
    get_forms,
    get_informations_from_description_area_from_db,
    register_description_area,
    test
)

def mockup_controller(request, context, id_maquette):

    context = get_description_areas_from_db(context, id_maquette)

    context = get_forms(context)

    test(id_maquette)

    if request.method == 'POST':

        action_form = ActionForm(request.POST)

        if action_form.is_valid():

            action = action_form.cleaned_data.get('action')

            if action == 'add_comment_content':

                if register_description_area(request, id_maquette):

                    return True, 'mockup'

            elif action == 'selected_description_area':

                id_description_area = action_form.cleaned_data.get('django_object_id')

                context = get_informations_from_description_area_from_db(context, id_description_area)

                request.session.update({
                    'context': {
                        'informations_from_description_area_data': context['informations_from_description_area_data'],
                        'id_description_area': id_description_area
                    }
                })