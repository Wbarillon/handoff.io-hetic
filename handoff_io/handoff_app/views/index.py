from django.shortcuts import redirect, render

from handoff_app.views.controllers.authentication import authentication_controller
from handoff_app.views.controllers.home import home_controller
from handoff_app.views.controllers.mockup import mockup_controller


# Create your views here.

def home(request):
    template_name = 'webpages/home.html'

    if 'context' in request.session:
        context = request.session['context']
        del request.session['context']
    else:
        context = {

        }

    home_controller(request, context)

    return render(request, template_name, context)

def authentication(request):
    template_name = 'forms/authentication.html'

    if 'context' in request.session:
        context = request.session['context']
        del request.session['context']
    else:
        context = {

        }

    if authentication_controller(request, context) and 'home':
        return redirect('home')
    else:
        return render(request, template_name, context)

def mockup(request): # les variables d'urls dynamiques sont ici normalement
    # Stimulate a mockup already stored in db and gotten from url.
    id_maquette = 1

    template_name = 'webpages/mockup.html'

    if 'context' in request.session:
        context = request.session['context']
        del request.session['context']
    else:
        context = {

        }

    if mockup_controller(request, context, id_maquette) and 'mockup':
        return redirect('mockup')
    else:
        return render(request, template_name, context)