from django.db import connection

from handoff_app.models import (
    DescriptionArea,
    TextComment
)
from handoff_io.utils import dictfetchall


def get_description_areas(id_maquette):

    data = list(DescriptionArea.objects.filter(id_maquette = id_maquette).values('id', 'x_axis', 'y_axis', 'width', 'height'))

    return data

def get_informations_from_description_area(id_description_area):
    
    data = list(TextComment.objects.filter(id_description_area = id_description_area).values('comment_content'))

    return data

def test_db(id_maquette):
    
    query = '''
        select *
        from handoff_app_maquette
        where id = :id_maquette
    '''

    with connection.cursor() as cursor:
        cursor.execute(
            query,
            {
                'id_maquette': id_maquette
            }
        )

        data = dictfetchall(cursor)

    return data