import json

from django.db import transaction

from handoff_app.views.db_api.mockup import (
    get_description_areas,
    get_informations_from_description_area,
    test_db
)
from handoff_app.forms import (
    AudioInput,
    DescriptionAreaModelForm,
    TextCommentModelForm,
    TextInput
)
from handoff_app.models import (
    DescriptionArea,
    Maquette,
    TextComment
)
from handoff_io.utils import save_django_object


def get_description_areas_from_db(context, id_maquette):

    description_areas = get_description_areas(id_maquette)

    context.update({
        'description_areas': description_areas
    })

    return context

def get_forms(context):

    text_form = TextInput()

    audio_form = AudioInput()

    context.update({
        'text_form': text_form,
        'audio_form': audio_form
    })

    return context

def get_informations_from_description_area_from_db(context, id_description_area):

    informations_from_description_area_data = get_informations_from_description_area(id_description_area)

    context.update({
        'informations_from_description_area_data': informations_from_description_area_data
    })

    return context

def rectangle_already_exist(description_area_form):

    if DescriptionArea.objects.filter(
        id_maquette = description_area_form['id_maquette'],
        x_axis = description_area_form['x_axis'],
        y_axis = description_area_form['y_axis'],
        height = description_area_form['height'],
        width = description_area_form['width']
    ).exists():
        return True
    else:
        return False

def register_description_area(request, id_maquette):

    url_variables = {
        'id_maquette': id_maquette
    }

    if request.POST.get('rectangleLocalization') != '':

        rectangleLocalization_jsonified = request.POST.get('rectangleLocalization')
        rectangleLocalization_dict = json.loads(rectangleLocalization_jsonified)

        data = {**url_variables, **rectangleLocalization_dict}

        description_area_form = DescriptionAreaModelForm(data)

        if description_area_form.is_valid():

            if not rectangle_already_exist(description_area_form.cleaned_data):

                with transaction.atomic():

                    description_area_instance = save_django_object(DescriptionArea(), description_area_form.cleaned_data)

                    data = {
                        'id_description_area': description_area_instance,
                        'comment_content': request.POST.get('description')
                    }

                    text_comment_form = TextCommentModelForm(data)

                    if text_comment_form.is_valid():

                        save_django_object(TextComment(), text_comment_form.cleaned_data)

                return True

            elif rectangle_already_exist(description_area_form.cleaned_data):
                id_description = int(request.POST.get('django_object_id'))
                data = {
                    'id_description_area': DescriptionArea.objects.get(id = id_description),
                    'comment_content': request.POST.get('description')
                }

                text_comment_form = TextCommentModelForm(data)

                if text_comment_form.is_valid():

                    save_django_object(TextComment(), text_comment_form.cleaned_data)
                    
                return True

def test(id_maquette):

    test = test_db(id_maquette)