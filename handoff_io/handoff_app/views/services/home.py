def reteste(context):

    coucou_retest = "coucou retest"

    context.update({
        'coucou_retest': coucou_retest
    })

    return context

def teste(context):

    coucou_test = "coucou test"

    context.update({
        'coucou_test': coucou_test
    })

    return context