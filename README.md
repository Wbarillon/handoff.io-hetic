Cloner le projet à partir de la branche develop.

Pour lancer le projet :
- installer Python 3.9.7
- créer un environnement virtuel sur votre ordinateur
- installer les dépendances

Création de l'environnement virtuel
- sélectionner l'endroit où sera l'environnement virtuel (racine de User par exemple)
- ouvrir un terminal à cet emplacement
- taper pour mac : python3 -m venv <nom de l'environnement>

Activer l'environnement virtuel
- à l'emplacement de l'environnement virtuel, taper : . bin/activate
- si l'environnement est activé, vous devriez voir son nom tout à gauche entre parenthèses

Installer les dépendances
- depuis le même terminal, se rendre depuis le dossier du repo git
- se rendre au même niveau que le fichier "requirements.txt"
- taper pip install -r requirements.txt

Si ça marche pas, taper pip install Django==3.1.13

Lancer le projet
- se rendre au même niveau que le fichier "manage.py"
- taper les commandes suivantes :
- python3 manage.py makemigrations
- python3 manage.py migrate
- python3 manage.py runserver