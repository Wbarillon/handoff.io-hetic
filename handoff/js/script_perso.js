

// var canvas = document.getElementById('canvas1');
// var ctx = canvas.getContext('2d');
// ctx.canvas.width  = document.getElementById('mockup').width;
// ctx.canvas.height  = document.getElementById('mockup').height;
// console.log(document.getElementById('mockup').width);


// function rectangle(coorX, coorY)
// {
// 	console.log(coorX+" "+coorY);
// 	ctx.rect(coorX, coorY, 10, 10);
// 	ctx.fill();
// }

// document.onclick = function(click)
// {
// 	if (click === undefined) click = window.click;
// 	let target= 'target' in click ? click.target : click.srcElement;

// 	if(target.tagName == 'CANVAS')
// 	console.log("Coordonées"+click.clientX+" "+click.clientY);

// 	rectangle(click.clientX-280, click.clientY-50);
// };


// get references to the canvas and context
var canvas = document.getElementById("canvas1");
var ctx = canvas.getContext("2d");

ctx.canvas.width  = document.getElementById('mockup').width;
ctx.canvas.height  = document.getElementById('mockup').height;

// style the context
ctx.strokeStyle = "#D87AFA";
ctx.lineWidth = 2;
ctx.shadowColor = '#7E42FF';
ctx.shadowBlur = 6;
ctx.shadowOffsetY = "3";

// calculate where the canvas is on the window
// (used to help calculate mouseX/mouseY)
var $canvas = $("#canvas1");
var canvasOffset = $canvas.offset();
var offsetX = canvasOffset.left;
var offsetY = canvasOffset.top;
var scrollX = $canvas.scrollLeft();
var scrollY = $canvas.scrollTop();

// this flage is true when the user is dragging the mouse
var isDown = false;

// these vars will hold the starting mouse position
var startX;
var startY;

function update(dash) {
    if (dash == 8) {
      dash += 1;
    }
    if (dash == 9) {
      dash -= 1;
    }
    return dash
}

function handleMouseDown(e) {
    e.preventDefault();
    e.stopPropagation();

    // save the starting x/y of the rectangle
    startX = parseInt(e.clientX - offsetX);
    startY = parseInt(e.clientY - offsetY);


    console.log("départ : " + startX + "," + startY);
    // set a flag indicating the drag has begun
    isDown = true;
}

function handleMouseUp(e) {
    e.preventDefault();
    e.stopPropagation();

    // the drag is over, clear the dragging flag
    isDown = false;
}

function handleMouseOut(e) {
    e.preventDefault();
    e.stopPropagation();

    // the drag is over, clear the dragging flag
    isDown = false;
}



function handleMouseMove(e) {
    e.preventDefault();
    e.stopPropagation();

    // if we're not dragging, just return
    if (!isDown) {
        return;
    }

    // get the current mouse position
    mouseX = parseInt(e.clientX - offsetX);
    mouseY = parseInt(e.clientY - offsetY);

    console.log("arrivée : " + mouseX + "," + mouseY);


    // Put your mousemove stuff here

    // clear the canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // calculate the rectangle width/height based
    // on starting vs current mouse position
    var width = mouseX - startX;
    var height = mouseY - startY;

    console.log("width : " + width + ", height : " + height);
var dash = 8;
    // draw a new rect from the start position 
    // to the current mouse position
    ctx.strokeRect(startX, startY, width, height);
    
    ctx.setLineDash([dash, 8]);
    // for (var i = 0; i <= 50; i++) {
    //     update(dash);
    //     setTimeout(
    //         ctx.setLineDash([dash, 8])
    //     , 500); 
    //     console.log('test');
    // }


}

// listen for mouse events
$("#canvas1").mousedown(function (e) {
    handleMouseDown(e);
});
$("#canvas1").mousemove(function (e) {
    handleMouseMove(e);
});
$("#canvas1").mouseup(function (e) {
    handleMouseUp(e);
});
$("#canvas1").mouseout(function (e) {
    handleMouseOut(e);
});